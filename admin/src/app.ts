import express from 'express'
import cors from 'cors'
import { Connection, createConnection } from 'typeorm'
import { Product } from './entity/product'
import * as amqp from 'amqplib/callback_api'

const port = 8000

createConnection().then(db => {
    const app = express()
    const productRepository = db.getRepository(Product)

    amqp.connect('amqps://cgzyzbbc:Jl6TLh_YAeM7yKr-BD-29UPgbePE3utq@tiger.rmq.cloudamqp.com/cgzyzbbc', function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if(error1){
                throw error1;
            }
            app.use(cors({
                origin: ['http://localhost:3000', 'http://localhost:8080', 'http://localhost:4200',]
            }))

            app.use(express.json())

            app.get('/api/products', async (req, res) => {
                const products = await productRepository.find()
                channel.sendToQueue('products', Buffer.from('hello'))
                res.json(products)
            })

            app.post('/api/products', async (req, res) => {
                const product = await productRepository.create(req.body)
                const result = await productRepository.save(product)
                channel.sendToQueue('product_created', Buffer.from(JSON.stringify(result)))
                return res.send(result)
            })

            app.get('/api/products/:id', async (req, res) => {
                const product = await productRepository.findOne(req.params.id)
                channel.sendToQueue('produc', Buffer.from(JSON.stringify(product)))
                return res.send(product)
            })

            app.put('/api/products/:id', async (req, res) => {
                const product: any = await productRepository.findOne(req.params.id)
                productRepository.merge(product, req.body)
                const result = await productRepository.save(product)
                channel.sendToQueue('product_updated', Buffer.from(JSON.stringify(result)))
                return res.send(result)
            })

            app.delete('/api/products/:id', async (req, res) => {
                const product = await productRepository.delete(req.params.id)
                channel.sendToQueue('product_deleted', Buffer.from(req.params.id))
                return res.send(product)
            })

            app.post('/api/products/:id/like', async (req, res) => {
                const product: any = await productRepository.findOne(req.params.id)
                product.likes++
                const result = await productRepository.save(product)
                return res.send(result)
            })

            app.listen(port, () => {
                console.log(`app listening at http://localhost:8000`)
            })
            process.on('beforeExit', () => {
                console.log('closing');
                connection.close()
            })
        });
    });
})

