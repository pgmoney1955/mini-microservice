"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const typeorm_1 = require("typeorm");
const product_1 = require("./entity/product");
const amqp = __importStar(require("amqplib/callback_api"));
const port = 8000;
(0, typeorm_1.createConnection)().then(db => {
    const app = (0, express_1.default)();
    const productRepository = db.getRepository(product_1.Product);
    amqp.connect('amqps://cgzyzbbc:Jl6TLh_YAeM7yKr-BD-29UPgbePE3utq@tiger.rmq.cloudamqp.com/cgzyzbbc', function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }
            app.use((0, cors_1.default)({
                origin: ['http://localhost:3000', 'http://localhost:8080', 'http://localhost:4200',]
            }));
            app.use(express_1.default.json());
            app.get('/api/products', (req, res) => __awaiter(this, void 0, void 0, function* () {
                const products = yield productRepository.find();
                channel.sendToQueue('products', Buffer.from('hello'));
                res.json(products);
            }));
            app.post('/api/products', (req, res) => __awaiter(this, void 0, void 0, function* () {
                const product = yield productRepository.create(req.body);
                const result = yield productRepository.save(product);
                channel.sendToQueue('product_created', Buffer.from(JSON.stringify(result)));
                return res.send(result);
            }));
            app.get('/api/products/:id', (req, res) => __awaiter(this, void 0, void 0, function* () {
                const product = yield productRepository.findOne(req.params.id);
                channel.sendToQueue('produc', Buffer.from(JSON.stringify(product)));
                return res.send(product);
            }));
            app.put('/api/products/:id', (req, res) => __awaiter(this, void 0, void 0, function* () {
                const product = yield productRepository.findOne(req.params.id);
                productRepository.merge(product, req.body);
                const result = yield productRepository.save(product);
                channel.sendToQueue('product_updated', Buffer.from(JSON.stringify(result)));
                return res.send(result);
            }));
            app.delete('/api/products/:id', (req, res) => __awaiter(this, void 0, void 0, function* () {
                const product = yield productRepository.delete(req.params.id);
                channel.sendToQueue('product_deleted', Buffer.from(req.params.id));
                return res.send(product);
            }));
            app.post('/api/products/:id/like', (req, res) => __awaiter(this, void 0, void 0, function* () {
                const product = yield productRepository.findOne(req.params.id);
                product.likes++;
                const result = yield productRepository.save(product);
                return res.send(result);
            }));
            app.listen(port, () => {
                console.log(`app listening at http://localhost:8000`);
            });
            process.on('beforeExit', () => {
                console.log('closing');
                connection.close();
            });
        });
    });
});
