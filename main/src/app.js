"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const typeorm_1 = require("typeorm");
const amqp = __importStar(require("amqplib/callback_api"));
const product_1 = require("./entity/product");
const axios_1 = __importDefault(require("axios"));
(0, typeorm_1.createConnection)().then(db => {
    const productRepository = db.getRepository(product_1.Product);
    amqp.connect('amqps://cgzyzbbc:Jl6TLh_YAeM7yKr-BD-29UPgbePE3utq@tiger.rmq.cloudamqp.com/cgzyzbbc', function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }
            channel.assertQueue('product_created', { durable: false });
            channel.assertQueue('product_updated', { durable: false });
            channel.assertQueue('product_deleted', { durable: false });
            const port = 8001;
            const app = (0, express_1.default)();
            app.use((0, cors_1.default)({
                origin: ['http://localhost:3000', 'http://localhost:8080', 'http://localhost:4200',]
            }));
            app.use(express_1.default.json());
            channel.consume('product_created', (msg) => __awaiter(this, void 0, void 0, function* () {
                const eventProduct = JSON.parse(msg === null || msg === void 0 ? void 0 : msg.content.toString());
                const product = new product_1.Product();
                product.admin_id = parseInt(eventProduct.id);
                product.title = eventProduct.title;
                product.image = eventProduct.image;
                product.likes = eventProduct.likes;
                yield productRepository.save(product);
                console.log('product created');
            }), {
                noAck: true
            });
            channel.consume('product_updated', (msg) => __awaiter(this, void 0, void 0, function* () {
                const eventProduct = JSON.parse(msg === null || msg === void 0 ? void 0 : msg.content.toString());
                const product = yield productRepository.findOne({ admin_id: parseInt(eventProduct.id) });
                productRepository.merge(product, {
                    title: eventProduct.title,
                    image: eventProduct.image,
                    likes: parseInt(eventProduct.likes.toString())
                });
                yield productRepository.save(product);
                console.log('product updated');
            }), {
                noAck: true
            });
            channel.consume('product_deleted', (msg) => __awaiter(this, void 0, void 0, function* () {
                const admin_id = parseInt(msg.content.toString());
                yield productRepository.delete({ admin_id });
                console.log('product deleted');
            }), {
                noAck: true
            });
            app.get('/api/products', (req, res) => __awaiter(this, void 0, void 0, function* () {
                const products = yield productRepository.find();
                return res.send(products);
            }));
            app.post('/api/products/:id/like', (req, res) => __awaiter(this, void 0, void 0, function* () {
                const product = yield productRepository.findOne(req.params.id);
                yield axios_1.default.post(`http://localhost:8000/api/products/${product === null || product === void 0 ? void 0 : product.admin_id}/like`, {});
                product.likes++;
                yield productRepository.save(product);
                return res.send(product);
            }));
            app.listen(port, () => {
                console.log(`app listening at http://localhost:${port}`);
            });
            process.on('beforeExit', () => {
                console.log('closing');
                connection.close();
            });
        });
    });
});
